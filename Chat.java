package ChatP2P;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Objects;
import java.util.Scanner;

import static ChatP2P.Chat.*;

/**
 * Created by evgenijpopov on 15.06.17.
 */
public class Chat {
    static final int SERVERPORT = 7777;
    static final String HOST = "127.0.0.1";
    //    static final String HOST = "10.240.21.221";
    static final String HEARTBEAT_MSG = "!HEARTBEAT!";
    static volatile Socket socket;
    static volatile InputStream inputStream;
    static volatile OutputStream outputStream;
    static volatile boolean isStop = false;
    static volatile boolean isDisconnected = false;
    static boolean isServer = true;

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length > 0 && args[0].equalsIgnoreCase("client"))
            isServer = false;

        String isServerMsg = isServer ? "Server" : "Client";
        System.out.printf("%s started..\n", isServerMsg);

        ServerSocket serverSocket = null;

        do {
            if (isServer) {
                serverSocket = new ServerSocket(SERVERPORT);
                socket = serverSocket.accept();
                System.out.println("Client connected..");
            } else {
                do {
                    try {
                        socket = new Socket(HOST, SERVERPORT);
                    } catch (IOException e) {
                        System.out.println("Error connecting as client. Retry after 1 second..");
                        Thread.sleep(1000);
                    }
                } while (socket == null);
                System.out.println("Connected to server.");
            }

            isDisconnected = false;
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
            (new Thread(new Lisener())).start();
            Thread sender = new Thread(new Sender());
            sender.start();
            (new Thread(new HeartBeat())).start();

            while (!isDisconnected && !isStop) {
                Thread.sleep(500);
            }
            if (isServer && serverSocket != null && !serverSocket.isClosed()) serverSocket.close();
            System.out.println("Socket was closed. Reconnecting.");
        } while (!isStop);
    }
}

class Lisener implements Runnable {
    static volatile long heartbeat = 0;

    @Override
    public void run() {
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        String msg;
        while (!isDisconnected) {
            try {
                if (inputStream.available() > 0) {
                    msg = dataInputStream.readUTF();
                    if (!msg.equals(HEARTBEAT_MSG)) System.out.printf("Get msg > %s\n", msg);
                    heartbeat = 0;
                }
                Thread.sleep(100);
            } catch (IOException | InterruptedException e) {
                System.out.printf("Exeption in Lisener: %s\n", e.getMessage());
            }
            if (heartbeat > 10) {
                System.out.println("No heartbeat");
                isDisconnected = true;
            }
            heartbeat++;
        }
        System.out.println("Lisener terminated.");
    }
}

class Sender implements Runnable {
    @Override
    public void run() {
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        Scanner scanner = new Scanner(System.in);
        String msg;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (!isDisconnected) {
            try {
                if (!br.ready()) continue;
                msg = scanner.nextLine();
                if (msg.equalsIgnoreCase("Bay")) {
                    isStop = true;
                    isDisconnected = true;
                }
                dataOutputStream.writeUTF(msg);
                dataOutputStream.flush();
            } catch (IOException e) {
                System.out.printf("Exeption in Sender: %s\n", e.getMessage());
                isDisconnected = true;
            }
        }
        System.out.println("Sender terminated.");
    }
}

class HeartBeat implements Runnable {
    @Override
    public void run() {
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

        while (!isDisconnected) {
            try {
                dataOutputStream.writeUTF(HEARTBEAT_MSG);
                dataOutputStream.flush();
                Thread.sleep(100);
            } catch (IOException | InterruptedException e) {
                System.out.printf("Exeption in HeartBeat: %s\n", e.getMessage());
                isDisconnected = true;
            }
        }
        System.out.println("HeartBeat terminated.");
    }
}
